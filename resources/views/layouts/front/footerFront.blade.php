
	<!-- Footer -->
	<footer class="footer">
		<div class="container">
			<div class="row">

				<div class="col-lg-3 footer_col">
					<div class="footer_column footer_contact">
						<div class="logo_container">
							<div class="logo"><a href="#"> NSH international School</a></div>
						</div>
						<div class="footer_title">Got Question? Call Us 24/7</div>
						<div class="footer_phone">+852-94169123</div>
						<div class="footer_contact_text">
							<p>Thamel Kathmandu, Nepal</p>
							<p>+977-1-4364419<br>+977-9851013932<br>+977-9849779322</p>
						</div>
						<div class="footer_social">
							<ul>
								<li><a href="https://www.facebook.com/naturalsoundhealing/?eid=ARBZcXh-kR8W8XArODTW8Oz7KqlTe1o_lgd7kqGzIT83zNhzu9ngetS4qxplCnkB8iUgysyiffQCbep_"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="nshis.com"><i class="fab fa-weixin"></i></a></li>
								<li><a href="nshis.com"><i class="fab fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-lg-2 offset-lg-3">
					<div class="footer_column">
						<div class="footer_title">Quick Menu</div>
						<ul class="footer_list">
							@foreach($category as $categorys)
							<li>-> <a href="{{ asset('/product/' .$categorys->id)}}">{{$categorys->category_name}}</a></li>
							@endforeach
						
						</ul>
					
					</div>
				</div>


				<div class="col-lg-2 col-sm-2 col-md-2">
					<div class="footer_column">
						<div class="footer_title">Our Branch</div>
						<ul class="footer_list">
							<li><p style="color: white">Flat E1 9/F Hanyee Building,19-21 <br>Road,Tsim sha Tsui Kowloon, Hongkong<br>Phone:- +852-969123</li>
							
						</ul>
						
					</div>
				</div>


				<div class="col-lg-2 col-sm-2 col-md-2">
					<div class="footer_column">
						<div class="footer_title">Account Info</div>
						<ul class="footer_list">
							<li><a href="{{asset('/customerRegister')}}">Sign Up</a></li>
							<li><a href="{{asset('customerLogin')}}">Login</a></li>
							<li><a href="{{asset('customerLogin')}}">My Account</a></li>
							<li><a href="{{asset('shopping_cart')}}">Cart</a></li>
							<li><a href="{{asset('termAndCondition')}}">Terms And Condition</a></li>
						</ul>
					</div>
				</div>

				

			</div>
		</div>
	</footer>

	<!-- payments -->

	<div class="copyright" style="background-color: #D63218">
		<div class="container" >
			<div class="row" >
				<div class="col-lg-3">
					
				</div>
				<div class="col-lg-6" align="center" >
					<div class="payment_title" >Payment Methods</div>
						<img src="{{asset('asset/image/visa.jpg')}}" alt="" style="height: 40px; width: 100px;">
						<img src="{{asset('asset/image/paypal.jpg')}}" alt="" style="height: 40px; width: 100px;">
						<img src="{{asset('asset/image/mastercard.jpg')}}" alt="" style="height: 40px; width: 100px;">
						
				</div>
				<div class="col-lg-3"></div>
			</div>
		</div>
	</div>



	<!-- Copyright -->

	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					
					<div class="copyright_container d-flex flex-sm-row flex-column align-items-center justify-content-start">
						<div class="copyright_content" align="center">
						<a>Copyright &copy; All rights reserved | This template is made by Hesienberg Information Technology</a>
						</div>
						
					</div>
				</div>
				<div class="col-lg-4"></div>
			</div>
		</div>
	</div>
</div>