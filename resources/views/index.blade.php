@extends('layouts.front.appFront')

@include('layouts.front.headerFront')

@section('content')

@if(Session::has('error'))

{{Session::get('error')}}

@endif

		<!-- Banner -->
  <div class="slider-content">
	<div class="row">
	<div class="col-sm-3 col-md-3">
		
	</div>
		<div class="col-sm-7 col-md-7">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
						  
						  <div class="carousel-inner">
						     <div class="carousel-item active ">
                              <img class="d-block w-100" src="sliderImage/1537522445.jpg"  alt="First slide">

                            </div>
                            @foreach($slider as $sliders)
                            <?php $url=url('sliderImage/'.$sliders->image); 
                                ?>

						    <div class="carousel-item ">
                              <img class="d-block w-100" src="{{$url}}"  alt="First slide">

                            </div>
                            @endforeach()
						    
						  </div>
						  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
					  </div>
		</div>
		<div class="col-sm-2 col-md-2"> 
			<div class="slider-add">
                <?php $url=url('galleryImage/'.'Banner_First.gif'); 
                                ?>
                                <?php $urls=url('galleryImage/'.'Basanta.jpg'); 
                                ?>
		<img class="img-fluid" src="{{$url}}" alt="animated">
        <img class="img-fluid" src="{{$urls}}" alt="animated">
		</div>
		</div>
		</div>
	</div>
	<div class="content">
		<div class="container">
		<div class="col-md-12 section-title">
                    <h2>SERVICES</h2>
        </div> <!-- /.section -->
        </div>
        <div class="row">
        	<div class="col-sm-12 col-md-12">
        		<div class="services">
                    <div id="owl-demo" class="owl-carousel owl-theme" align="center;">
                        @foreach($service as $services)

                        <?php $url=url('serviceImage/'.$services->image); 
                            
                                ?>

                <div class="item"><a href="{{ asset('/servicedetail/' .$services->id)}}"><img src="{{$url}}" style="height: 200px; width: 250px;"></a></div>

                         @endforeach()
        		 

                  </div>
        		</div>
        	</div>
    	</div>


	</div>

	<div class="content">
		<div class="container">
		<div class="col-md-12 section-title">
                    <h2>top product</h2>
        </div> <!-- /.section -->
        </div>
        <div class="container-fluid">
        	<div class="row">
        		<div class="col-sm-12 col-md-12 col-lg-9">
        			<!-- Top Start hear -->
        			 <section class="newproduct bgwhite p-t-45 p-b-105">
        <div class="row" style="margin-bottom: 50px;">

@foreach($product as $products)
    <div class="col-sm-3 col-md-3"> 
                        <div class="wrap-slick2">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <div class="block2">
                            <figure class="snip1524">
                                <?php $url=url('productImage/'.$products->product_image); 
                            
                                ?>
                                <div class="block2-img wrap-pic-w of-hidden pos-relative ">
                               <img src="{{$url}}" alt="IMG-PRODUCT" style="width: 450px; height: 300px" >

                                
                            </div>
                              <figcaption>
                                <div class="block2-overlay trans-0-4">
                                    



                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                        <a href="{{route('addToCart' ,$products->id)}}" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                                            Add to Cart
                                        </a>
                                    </div>
                                    <a href="{{asset('/productDetail/'.$products->id)}}" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>
                                <p style="margin-top: 50px;">{{$products->product_short_description}}</p>
                                </div>
                              </figcaption>
                              <a href="{{asset('/productDetail/'.$products->id)}}"></a>
                            </figure>
                            

                            <div class="block2-txt p-t-20">
                                <a href="{{asset('/productDetail/'.$products->id)}}" class="block2-name dis-block s-text3 p-b-5">
                                    {{$products->product_name}}
                                </a>

                                <span class="block2-price m-text6 p-r-5">
                                    ${{$products->product_normal_price}}.00
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        
    </div>
    @endforeach 
</section>



        			<!-- End top product -->
        		</div>
        		<div class="col-sm-12 col-md-12 col-lg-3 " style="padding:4% 4% 4% 4%;">
        			<div class="row">
        				<div class="col-md-6 col-sm-6 col-lg-12">
        				<section class="newproduct bgwhite p-b-105">
						<div class="row">
							<div class="single-sidebar-widget ads-widget">
                                <?php $url=url('galleryImage/'.'Banner_Second.gif'); 
                                ?>
								<img class="img-fluid" src="{{$url}}" alt="animated">
							</div>
						</div>
					</section>
        			</div>
        			
					<div class="col-md-6 col-sm-6 col-lg-12">
        				<section class="newproduct bgwhite p-b-105">
						<div class="row">
							<div class="single-sidebar-widget ads-widget">
                                <?php $url=url('galleryImage/'.' Banner_Third.gif'); 
                                ?>
								<img class="img-fluid" src="{{$url}}" alt="animated">
							</div>
						</div>
					</section>
        			</div>
        			</div>
        			
        		</div>
        	</div>
		</div>
	</div>

	<div class="content">
		<div class="container-fluid">

			 <!--Category Product Start  -->

              <div class="row">
        <div class="col-sm-12 col-md-6 ">
            <div class="container">
            <div class="col-md-12 section-title">
                        <h2>Cushions</h2>
            </div> <!-- /.section -->
            </div>


    <section class="newproduct bgwhite p-t-45 p-b-105" style="border-right: 1px solid #dddddd;">
        <div class="row">

            <!-- Loop Start Here -->
            @foreach($Cproduct as $Cproducts)

    <div class="col-sm-6 col-md-12 col-lg-6"> 


    
                    <div class="wrap-slick2">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <?php $url=url('productImage/'.$Cproducts->product_image); 
                            
                                ?>
                        <div class="block2">
                            <figure class="snip1524">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative ">
                                <img src="{{$url}}" alt="IMG-PRODUCT" style="width: 400px; height: 250px"">

                                
                            </div>
                              <figcaption>
                                <div class="block2-overlay trans-0-4">
                                    



                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                                            Add to Cart
                                        </button>
                                    </div>
                                    <a href="{{asset('/productDetail/'.$Cproducts->id)}}" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>
                                <p style="margin-top: 50px;">{{$Cproducts->product_short_description}} </p>
                                </div>
                              </figcaption>
                              <a href="{{route('addToCart' ,$products->id)}}"></a>
                            </figure>
                            

                            <div class="block2-txt p-t-20">
                                <a href="{{asset('/productDetail/'.$Cproducts->id)}}" class="block2-name dis-block s-text3 p-b-5">
                                    {{$Cproducts->product_name}}
                                </a>

                                <span class="block2-price m-text6 p-r-5">
                                    ${{$Cproducts->product_normal_price}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
    

    </div>
    @endforeach
    <!-- Loop End Here -->

            </div>
        </section>
     </div>



        <div class="col-sm-12 col-md-6">
            <div class="container">
            <div class="col-md-12 section-title">
                        <h2>Bells</h2>
            </div> <!-- /.section -->
            </div>

            <section class="newproduct bgwhite p-t-45 p-b-105">
        <div class="row" >
            <!--Loop Start Here --> 
            @foreach($Coproduct as $Coproducts)
    <div  class="col-sm-6 col-md-12 col-lg-6"> 


    
                    <div class="wrap-slick2">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <?php $url=url('productImage/'.$Coproducts->product_image); 
                            
                                ?>
                        <div class="block2">
                            <figure class="snip1524">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative ">
                                <img src="{{$url}}" alt="IMG-PRODUCT" style="width: 400px; height: 250px">

                                
                            </div>
                              <figcaption>
                                <div class="block2-overlay trans-0-4">
                                    



                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                                            Add to Cart
                                        </button>
                                    </div>
                                    <a href="{{asset('/productDetail/'.$Coproducts->id)}}" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>
                                <p style="margin-top: 50px;">{{$Coproducts->product_short_description}} </p>
                                </div>
                              </figcaption>
                              <a href="{{route('addToCart' ,$products->id)}}"></a>
                            </figure>
                            

                            <div class="block2-txt p-t-20">
                                <a href="{{asset('/productDetail/'.$Coproducts->id)}}" class="block2-name dis-block s-text3 p-b-5">
                                    {{$Coproducts->product_name}}
                                </a>

                                <span class="block2-price m-text6 p-r-5">
                                    ${{$Coproducts->product_normal_price}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
    

    </div>

    <!--Loop End Hear -->

    @endforeach

    
    </div>
</section>


        </div>
    </div>

             <!-- End Category Product   -->
		</div>
	</div>

	<div class="content">
		<div class="container">
		<div class="col-md-12 section-title">
                    <h2>recent product</h2>
        </div> <!-- /.section -->
        </div>
        <div class="container-fluid">
        	<div class="row">
        		<div class="col-sm-12 col-md-12 col-lg-9">
        		 <!-- Recent Product Start -->


                    <section class="newproduct bgwhite p-t-45 p-b-105">
        <div class="row" style="margin-bottom: 50px;">

@foreach($recentproduct as $products)
    <div class="col-sm-3 col-md-3"> 
                        <div class="wrap-slick2">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <div class="block2">
                            <figure class="snip1524">
                                <?php $url=url('productImage/'.$products->product_image); 
                            
                                ?>
                                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                                <img src="{{$url}}" alt="IMG-PRODUCT" style="width: 400px; height: 250px" >

                                
                            </div>
                              <figcaption>
                                <div class="block2-overlay trans-0-4">
                                    



                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                                            Add to Cart
                                        </button>
                                    </div>
                                    <a href="{{asset('/productDetail/'.$products->id)}}" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>
                                <p style="margin-top: 50px;">{{$products->product_short_description}}</p>
                                </div>
                              </figcaption>
                              <a href="{{route('addToCart' ,$products->id)}}"></a>
                            </figure>
                            

                            <div class="block2-txt p-t-20">
                                <a href="{{asset('/productDetail/'.$products->id)}}" class="block2-name dis-block s-text3 p-b-5">
                                    {{$products->product_name}}
                                </a>

                                <span class="block2-price m-text6 p-r-5">
                                    ${{$products->product_normal_price}}.00
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        
    </div>
    @endforeach 
</section>

               <!-- End Recent Product -->
        		</div>
        		<div class="col-sm-12 col-md-12 col-lg-3 " style="padding:4% 4% 4% 4%;">
        			<div class="row">
        				<div class="col-md-6 col-sm-6 col-lg-12">
        				<section class="newproduct bgwhite p-b-105">
						<div class="row">
							<div class="single-sidebar-widget ads-widget">
							    <?php $url=url('galleryImage/'.'Banner_Fourth.jpg'); 
                                ?>
								<img class="img-fluid" src="{{$url}}" alt="">
							</div>
						</div>
					</section>
        			</div>
        			
					<div class="col-md-6 col-sm-6 col-lg-12">
        				<section class="newproduct bgwhite p-b-105">
						<div class="row">
							<div class="single-sidebar-widget ads-widget">
							    <?php $url=url('galleryImage/'.'Banner_Fifth.jpg'); 
                                ?>
								<img class="img-fluid" src="{{$url}}" alt="">
							</div>
						</div>
					</section>
        			</div>
        			</div>
        			
        		</div>
        	</div>
		</div>
	</div>


	<div class="content">
		<div class="container-fluid">
			 <!--Category Product Start  -->

              <div class="row">
        <div class="col-sm-12 col-md-6 ">
            <div class="container">
            <div class="col-md-12 section-title">
                        <h2>Mix Product</h2>
            </div> <!-- /.section -->
            </div>


    <section class="newproduct bgwhite p-t-45 p-b-105" style="border-right: 1px solid #dddddd;">
        <div class="row">

            <!-- Loop Start Here -->
            @foreach($Co5product as $Cproducts)

    <div class="col-sm-6 col-md-12 col-lg-6"> 


    
                    <div class="wrap-slick2">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <?php $url=url('productImage/'.$Cproducts->product_image); 
                            
                                ?>
                        <div class="block2">
                            <figure class="snip1524">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative ">
                                <img src="{{$url}}" alt="IMG-PRODUCT" style="width: 400px; height: 250px">

                                
                            </div>
                              <figcaption>
                                <div class="block2-overlay trans-0-4">
                                    



                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                                            Add to Cart
                                        </button>
                                    </div>
                                    <a href="{{asset('/productDetail/'.$Cproducts->id)}}" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>
                                <p style="margin-top: 50px;">{{$Cproducts->product_short_description}} </p>
                                </div>
                              </figcaption>
                              <a href="{{route('addToCart' ,$products->id)}}"></a>
                            </figure>
                            

                            <div class="block2-txt p-t-20">
                                <a href="{{asset('/productDetail/'.$Cproducts->id)}}" class="block2-name dis-block s-text3 p-b-5">
                                    {{$Cproducts->product_name}}
                                </a>

                                <span class="block2-price m-text6 p-r-5">
                                    ${{$Cproducts->product_normal_price}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
    

    </div>
    @endforeach
    <!-- Loop End Here -->

            </div>
        </section>
     </div>



        <div class="col-sm-12 col-md-6">
            <div class="container">
            <div class="col-md-12 section-title">
                        <h2>Mix Product</h2>
            </div> <!-- /.section -->
            </div>

            <section class="newproduct bgwhite p-t-45 p-b-105">
        <div class="row" >
            <!--Loop Start Here --> 
            @foreach($Co4product as $Coproducts)
    <div  class="col-sm-6 col-md-12 col-lg-6"> 


    
                    <div class="wrap-slick2">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <?php $url=url('productImage/'.$Coproducts->product_image); 
                            
                                ?>
                        <div class="block2">
                            <figure class="snip1524">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative ">
                                <img src="{{$url}}" alt="IMG-PRODUCT" style="width: 400px; height: 250px">

                                
                            </div>
                              <figcaption>
                                <div class="block2-overlay trans-0-4">
                                    



                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="border-color:transparent;">
                                            Add to Cart
                                        </button>
                                    </div>
                                    <a href="productDetail" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>
                                <p style="margin-top: 50px;">{{$Coproducts->product_short_description}} </p>
                                </div>
                              </figcaption>
                              <a href="{{route('addToCart' ,$products->id)}}"></a>
                            </figure>
                            

                            <div class="block2-txt p-t-20">
                                <a href="{{asset('/productDetail/'.$Coproducts->id)}}" class="block2-name dis-block s-text3 p-b-5">
                                    {{$Coproducts->product_name}}
                                </a>

                                <span class="block2-price m-text6 p-r-5">
                                    ${{$Coproducts->product_normal_price}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
    

    </div>

    <!--Loop End Hear -->

    @endforeach

    
    </div>
</section>


        </div>
    </div>

             <!-- End Category Product   -->
		</div>
	</div>
	

	<div class="content">
		<div class="container">
		<div class="col-md-12 section-title">
                    <h2>Certified Course</h2>
        </div> <!-- /.section -->
        </div>
        <div class="container-fluid">
        	<div class="row">
        		<!--Book Display Hear -->
                 @foreach($book as $books)
                 <?php $url=url('bookImage/'.$books->image); 
                                ?>

                    <div class="col-sm-3 col-md-3">
                    <div class="product-item-4">
                        <div class="product-thumb">
                            <img src="{{$url}}" alt="Product Title" style="height: 300px; width: 300px;">
                        </div> <!-- /.product-thumb -->
                        <div class="product-content overlay">
                            <h5><a href="{{ asset('/booksDetail/' .$books->id)}}">{{$books->book_name}}</a></h5>
                            <span class="tagline">{{$books->short_description}}</span>
                            <span class="price"></span>
                        </div> <!-- /.product-content -->
                    </div> <!-- /.product-item-4 -->
                    </div>

@endforeach

                 <!--End Book Display  -->


        	</div>
		</div>
	</div>

	<div class="content">
		<div class="container">
		<div class="col-md-12 section-title">
                    <h2>Certified Student</h2>
        </div> <!-- /.section -->
        </div>
        <div class="container-fluid">
        	<div class="row">

        		<!-- Blog Start Hear -->
@foreach($blog as $blogs)


<div class="col-sm-3 col-md-3">
    <?php $url=url('blogImage/'.$blogs->image); 
                                ?>

   <div class="blog-card spring-fever" style=" background: {{$url}} center no-repeat;">
    <img src="{{$url}}" style="height: 300px; width: 300px;">
  <div class="title-content">
    <h3><a href="blogDetail">{{$blogs->blog_title}}</a></h3>
  </div>
  <div class="card-info">
    {{$blogs->blog_content}}
    <a href="{{ asset('/blogDetail/' .$blogs->id)}}">Read Article<span class="licon icon-arr icon-black"></span></a>
  </div>
  <div class="gradient-overlay"></div>
  <div class="color-overlay"></div>
</div><!-- /.blog-card -->
</div>
@endforeach
</div>



                <!-- End Blog -->
        	</div>
		</div>
	</div>

  @endsection